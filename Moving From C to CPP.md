# Moving from C to CPP

[Reference](https://docs.google.com/document/d/1SSPtJEyM6Ze4DePE-EBhIHYqb2kumUvpYXehmzKK2xQ/edit)

Headers Comparision

|C headers|C++ headers|
|---|---|
|stdio.h|iostream|
|string.h|included in iostream|
|math.h|algorithms.h or cmath|
|no header to include all| bits/stdc++.h|

Input output are the most importatnt when it comes to competetive coding

|C |C++|
|---|---|
|printf()|cout<<|
|scanf()|cin>>|
|no dynamic arays|vectors|
|no supprt for strings|strings library|
|pointers|iterators|
|`\n`|endl(slow because of flush)|

--- 
## Concept of namespace

``` c
#include<bits/stdc++.h>
using namespace std;

int main(){
	cout<<"Hello world!";
}
```

the namespace defices the class scope in cpp

we could write the programme even without namespace declaration but we need to define the scope (std::function_name) every time we use a function/variable from that class

```c
#include<bits/stdc++.h>
using namespace std;

int main(){
	std::cout<<"Hello world!";
}
```

- cin for string gets input with space or `\n` as delimiter getline(cin,s) gets input with the only delimiter of `\n`

