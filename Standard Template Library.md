# Standard Template Library
[Reference-1](https://www.topcoder.com/thrive/articles/Power%20up%20C++%20with%20the%20Standard%20Template%20Library%20Part%20One)
[Reference-2](https://www.topcoder.com/thrive/articles/Power%20up%20C++%20with%20the%20Standard%20Template%20Library%20Part%20Two:%20Advanced%20Uses)
- We have many libraries/functions/algorithms implemented within the cpp there are several containers and functions we can use.
## Containers
- [[StandardTemplateLibraryFolder/container]]
- [[StandardTemplateLibraryFolder/Vector]]
- [[StandardTemplateLibraryFolder/pairs]]
- [[StandardTemplateLibraryFolder/Iterators]]
- [[StandardTemplateLibraryFolder/Strings]]
- [[StandardTemplateLibraryFolder/Set]]
- [[StandardTemplateLibraryFolder/Map]]
- [[StandardTemplateLibraryFolder/Stack]]
- [[StandardTemplateLibraryFolder/Queue]]



## Functions
- [[StandardTemplateLibraryFolder/Minor_func]]