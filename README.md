# Group Notes
Good Books For Competetive Programming
[Hand Book](https://cses.fi/book/book.pdf)

## To-Do List
- [ ] STL `Due Date:29/09/21`
- [ ] Algorithms & Data Structures
- [ ] Java
- [ ] Advanced Data Structures AI algo upto A*
- [ ] DBMS, UNIX
- [ ] OS, OOPS
- [ ] Networks
- [ ] CSO

## Practice

- [ ] Codeforces
- [ ] Atcoder Dp Contest
- [ ] Leetcode
- [ ] cses probelmset
- [ ] Interviewbit Revision