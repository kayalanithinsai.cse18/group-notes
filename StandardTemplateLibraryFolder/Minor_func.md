# Useful Functions in STL

## Min & Max

```c++
#include <algorithm> // header for min, max
min(a,b); // returns minimum of a,b
max(a,b); // returns maximum of a,b
// make sure that a and b must be of same datatype
```

## Min element & Max element

```c++
min_element(begin(),end()); // returns the iterator pointing min position
max_element(begin(),end()); // returns the iterator pointing max position 
```

## Swap

```c++
a=10;
b=20;
swap(a,b); // changes a to 20 and b to 10 but be carefull while passing it to a function as they are not passed by reference.
```

## Sort

```c++
vector<int> v;
sort(v.begin(),v.end()); // sorts elemnts from begining to end in acsending fashion.

int a[n];
sort(a,a+n); // ascending order

sort(a, a+n, greater<int>()); // descinding order
```

For Custom sorting
```c++
bool func_name(type a, type b){
	// if comparision returns true then the first element would be first and second element would be later in sorted array.
}
sort(begin(),end(),func_name);
```

Time Complexity - O(N LogN)

## reverse
```c++
reverse(begin(),end()) // revetses all elements between mentioned iterators.
```

## Next_permutation & Prev_permutation
```c++
int arr[3] = { 1, 2, 3 };
do{
	cout<<arr[0]<<" "<<arr[1]<<" "<<arr[2]<<endl;
}while(next_permutation(arr,arr+3));
```
- similar use of previous_permutation

## lower_bound
binary search a **sorted arrar/vector**
returns the address/iterator of the first element where a[i] >= val 
```c++
vector<int> v{10,20,30,40,50};
lower_bound(v.begin(),v.end(),30); // returns pointer to 30.
vector<int> v{10,20,35,40,50};
lower_bound(v.begin(),v.end(),30); // returns pointer to 35.
vector<int> v{10,20,30,30,40,50};
lower_bound(v.begin(),v.end(),30); // returns pointer to first 30.
```

## upper_bound
Binary search **sorted array/vector**
returns the address/iterator of the first element where a[i] > val
```c++
vector<int> v{10,20,30,40,50};
upper_bound(v.begin(),v.end(),30); // returns pointer to 40.
vector<int> v{10,20,35,40,50}; 
upper_bound(v.begin(),v.end(),30); // returns pointer to 35.
vector<int> v{10,20,30,30,40,50};
```

