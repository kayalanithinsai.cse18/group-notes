# Stack

Stack is a data strucure which works on FILO and c++ stl has inbuilt implementation for a stack.

## Header
`#include<stack>`

## decleration
```c++
stack<int> s;
```

## Functions

```c++
stack<int> s;
s.push(10); // insert element 10 into the stack
s.push(20);
s.pop(); // remove the top most element
s.top(); // returns the top most element but does not removes the element
s.size(); // returns the size of the stack
s.empty(); // returns true if stack is empty.
```

