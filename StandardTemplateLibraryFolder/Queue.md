# Queue
Queue is a data structure which works on FIFO principle.

## Header

`#include<queue>`

## Decleration

```c++
queue<int> q;
```

## Functions

```c++
queue<int> q;
q.push(10); //inserts element 10 into the queue.
q.front(); returns the front element int the queue.
q.pop(); removes the first element from the queue.
q.size(); // retrnd th size of the queue.
q.empty(); // returns true if queue is empty.
```
