# vector
- It is a container
- it is dynamically allocated
## header
`#include <vector>`
## Decleration
```c
// geberal vector decleration
vector<int> v;	
```
we could also use partiular sized vector with following operation
```c
// for 1D vector of fixed size.
vector<int> v(10);
vector<int> v(10,1); // initialises vector with 1
vector<vector<int>> v;
// for 2D vectors
// some compilers do not accept
vector<vector<int>> v;
// instead use this
vector<vector<int> > v;
```
normal indexing can be used in vectors
`v[i]`

## Functions

```c
vector<int> v; // normal vector definition
v.push_back(1); // insert an element in vector at ending
v.size() // to get vector size and returns int
v.empty() // returns bool (true if vector is empty else false)
v.back() // to get back element
v.front() // to get first element
v[i] // to get element from index.
v.insert(i,5) //to insert 5 at position i all elements after i are moved by a step.
v.erase(iterator) // erases the value at iterator and shifts all later values fowward O(n) operation
v.erase(begin iterator, end iterator)
```


