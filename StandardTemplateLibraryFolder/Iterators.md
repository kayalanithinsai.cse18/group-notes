# Iterators
Because of the misuse of pointers several issues had arised as (crashing ogf the programme/memory leaks in security)
in order to resolve the problem in cpp we have an abstraction of pointers the iterators.

## Decleration

```c
vector<int> v;
int a[10]; 
vector<int>::iterator it;
vector<int>::iterator it = v.begin();
int::iterator = a;
// writing the iterator is legthy so we often use it as
auto it = v.begin();
```

## Functions & Ussage
- we can increment or decrement iterators `Ex: it++ ot it--` 
- iterator only supports addition and subtraction only
	- iterator allows only integer addition
	- for subtraction we can subtract 2 iterators

let us say we have a vector of size 10 we can have 2 types of iteration
Type-1
```c
vector<int> v(10);
for(int i=0;i<10;i++){
	cout<<v[i]<<" ";
}
cout<<endl;
```
Type-2
```c
vector<int> v(10)
for(int x:v){
	cout<<x<<" ";
}
cout<<endl;
// if indexing is not possible then we have to use iterators.
set<int> hash;
for(int x:hash){
	cout<<x<<" ";
}
```

### Find an element in a container

```c
find(begining_iterator,ending_iterator,valueto search) // this returs an iterator where it first matches if the iterator is end() then which means the given element does not exists.
```