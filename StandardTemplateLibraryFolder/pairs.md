# Pairs
- Container
- it is used to store 2 elements (maybe different as int and float)

Header `#include utility`

## Decleration
```c
pair<int,int> p;
pair<int,int> p={2,3};
```

## functions

```c
pair<int,int> p // decleration
p = make_pair(2,3); // this is standard way of initialisation
p = {2,3} // this is unstandard initialisation but it works
p.first // returns first element
p.second // returns second element

pair<int,pair<int,int> > p = {1,{2,3}}; // nested pairs
```

On sorting the first element is compared if the first element is same then the second element is comapred.

