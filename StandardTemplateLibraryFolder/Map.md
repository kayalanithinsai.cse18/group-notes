# Map
Map is a container which stores a key value pair.

## headers

`#include <map>`
`#include <unordered_map>`

## Declarations

```c++
map<int,int> mp;
map<string,int> mp;
unordered_map<int,int>;
```

## Functions

```c++
map<int,int> mp;
mp[2] = 3; // we store value 3 for key 2
mp[3]++; // if anykey is not initialised then it is value is 0 initially
```

- Normal iterators will work as `mp.begin()` and `mp.end()`
- `mp.size()` returns size of the map
- `mp.erase(key)` removes the key from map
- `mp.clear()` clears th entire map
- The difference between map and unordered_map is the time complexity it takes `O(logN)` for map and `O(1)` for unordered-map but in worst case senarios the time complexity can go to O(N<sup>2</sup>).
- `find()` works simillarly as t does for other containers.