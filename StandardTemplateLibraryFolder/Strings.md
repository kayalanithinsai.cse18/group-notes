# Strings

string is motly similar with `vector<char>` but there are few differences

## Declaration
```c
string s = "hello";
```
	
## Functions
all `vector<char>` functions works on strings
```c
s.substr(0,3); // substr time complexity= O(len of substr)
s.length(); // returns int length of string O(1)
s += s2; // time complexity = O(len(s2))
```