# Set

# Header

`#include<set>`
`#include<unordered_set>`

There are 3 implementations of sets
1. **ordered sets or known as set** here all data is stored is sorted order with a self balancing BST. Timecomplexity to insert and retrive is o(logn)
2. **unorderd_set** the data is store with hash table with hashing algotrithms. Time complexity is O(1) for insertion and retrive
3. **multiset** in the above set implementations we cannot add same value multiple times but we can do that with a multiset.

## Decleration

```c++
set<int> s; //decleration for ordered_set
unordered_set<int> s; // decleration for unordered_set
multiset<int> s;
```

## Functions
The same functions works for all implementations of sets

```c++
set<int> s;
s.insert(10); // To insert value 10 into the set
s.erase(10); // to erase value 10 from thwe set
s.count(10); // to find the count of set items
```

- Never use `find(begin iterator , end iterator)` for sets as find takes `O(N)` time complexity to find the element instead we can use `s.count(val) == 1`.
- Inorder to iterate the sets we can only use iterator as sets cannot be indexed.  
```c++
for(auto it=s.begin();it<s.end();it++){
	cout<<*it<" ";
}
```

```c++
s.size(); // returns the size of the set
s.empty(); // returns true if set is empty
```

- for the rerase function for multiset it removes all instances of the same value.
- `s.erase(a.find())` removes only one instance.

## Note

- It is always better to use `set` inteadof `unordered_set` as using unordered set could lead to chaining from hash collisions and results in TLE in contests/Exams.

